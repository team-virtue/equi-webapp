interface Appliance {
  power_consumption: number;
  time_start: Date;
  time_end: Date;
  device_type: string;
  size: number;
}
